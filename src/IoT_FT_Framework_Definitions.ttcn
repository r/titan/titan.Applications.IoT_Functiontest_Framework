///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2021 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               IoT_FT_Framework_Definitions.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 910
//  Updated:            2021-02-03
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module IoT_FT_Framework_Definitions
{
  import from IFW_Common all;
  import from IFW_CoAP_Peer_Definitions all;
  import from IFW_HTTP_Client_Definitions all;
  import from IFW_HTTP_Server_Definitions all;
  import from IFW_MQTT_Client_Definitions all;
  import from IFW_MQTT_Server_Definitions all;

  type component IFW_MAIN_CT
  extends IFW_BASE_CT
  {
    var IFW_COAP_CT_List coapPeers := {};
    var IFW_HTTP_Client_CT_List httpClients := {};
    var IFW_HTTP_Server_CT_List httpServers := {};
    var IFW_MQTT_Client_CT_List mqttClients := {};
    var IFW_MQTT_Server_CT_List mqttServers := {};
		
    var integer mid := 0;
  }
	
  type record of IFW_COAP_CT IFW_COAP_CT_List;
  type record of IFW_HTTP_Client_CT IFW_HTTP_Client_CT_List;
  type record of IFW_HTTP_Server_CT IFW_HTTP_Server_CT_List;
  type record of IFW_MQTT_Client_CT IFW_MQTT_Client_CT_List;
  type record of IFW_MQTT_Server_CT IFW_MQTT_Server_CT_List;

  type enumerated IFW_ComponentTypes
  {
    COAP_PEER,
    HTTP_CLIENT,
    HTTP_SERVER,
    MQTT_CLIENT,
    MQTT_SERVER
  }
}
